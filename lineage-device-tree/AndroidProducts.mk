#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_Infinix-X671B.mk

COMMON_LUNCH_CHOICES := \
    lineage_Infinix-X671B-user \
    lineage_Infinix-X671B-userdebug \
    lineage_Infinix-X671B-eng
